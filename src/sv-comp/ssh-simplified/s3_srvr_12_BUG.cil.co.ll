; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_12_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %1 = tail call i64 @nondet_2() nounwind
  %call.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %tobool253.i = icmp eq i32 %0, -2097152
  %mul279.i = shl nsw i32 %0, 3
  br label %while.body.i

while.body.i:                                     ; preds = %if.then559.i, %switch_1_break.i, %switch_1_break.i.thread, %while.body.i.preheader
  %2 = phi i64 [ %1, %while.body.i.preheader ], [ %2, %switch_1_break.i.thread ], [ %24, %switch_1_break.i ], [ %24, %if.then559.i ]
  %3 = phi i64 [ %1, %while.body.i.preheader ], [ %.ph42, %switch_1_break.i.thread ], [ %25, %switch_1_break.i ], [ %25, %if.then559.i ]
  %4 = phi i32 [ 0, %while.body.i.preheader ], [ %4, %switch_1_break.i.thread ], [ %26, %switch_1_break.i ], [ %26, %if.then559.i ]
  %.732 = phi i32 [ %0, %while.body.i.preheader ], [ %.729.ph, %switch_1_break.i.thread ], [ %.729, %switch_1_break.i ], [ %.729, %if.then559.i ]
  %.927 = phi i32 [ %0, %while.body.i.preheader ], [ %.927, %switch_1_break.i.thread ], [ %.928, %switch_1_break.i ], [ %.928, %if.then559.i ]
  %5 = phi i32 [ %0, %while.body.i.preheader ], [ %.ph43, %switch_1_break.i.thread ], [ %27, %switch_1_break.i ], [ %27, %if.then559.i ]
  %6 = phi i32 [ %0, %while.body.i.preheader ], [ %.ph44, %switch_1_break.i.thread ], [ %28, %switch_1_break.i ], [ %28, %if.then559.i ]
  %7 = phi i32 [ 8464, %while.body.i.preheader ], [ %.ph45, %switch_1_break.i.thread ], [ %29, %switch_1_break.i ], [ %29, %if.then559.i ]
  br label %NodeBlock130

NodeBlock130:                                     ; preds = %while.body.i
  %Pivot131 = icmp slt i32 %7, 8544
  br i1 %Pivot131, label %NodeBlock83, label %NodeBlock128

NodeBlock128:                                     ; preds = %NodeBlock130
  %Pivot129 = icmp slt i32 %7, 8640
  br i1 %Pivot129, label %NodeBlock106, label %NodeBlock126

NodeBlock126:                                     ; preds = %NodeBlock128
  %Pivot127 = icmp slt i32 %7, 12292
  br i1 %Pivot127, label %NodeBlock114, label %NodeBlock124

NodeBlock124:                                     ; preds = %NodeBlock126
  %Pivot125 = icmp slt i32 %7, 16384
  br i1 %Pivot125, label %LeafBlock116, label %NodeBlock122

NodeBlock122:                                     ; preds = %NodeBlock124
  %Pivot123 = icmp slt i32 %7, 24576
  br i1 %Pivot123, label %LeafBlock118, label %LeafBlock120

LeafBlock120:                                     ; preds = %NodeBlock122
  %SwitchLeaf121 = icmp eq i32 %7, 24576
  br i1 %SwitchLeaf121, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock118:                                     ; preds = %NodeBlock122
  %SwitchLeaf119 = icmp eq i32 %7, 16384
  br i1 %SwitchLeaf119, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock116:                                     ; preds = %NodeBlock124
  %SwitchLeaf117 = icmp eq i32 %7, 12292
  br i1 %SwitchLeaf117, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock114:                                     ; preds = %NodeBlock126
  %Pivot115 = icmp slt i32 %7, 8656
  br i1 %Pivot115, label %LeafBlock108, label %LeafBlock111

LeafBlock111:                                     ; preds = %NodeBlock114
  %.off112 = add i32 %7, -8656
  %SwitchLeaf113 = icmp ule i32 %.off112, 1
  br i1 %SwitchLeaf113, label %if.end424.i, label %NewDefault

LeafBlock108:                                     ; preds = %NodeBlock114
  %.off109 = add i32 %7, -8640
  %SwitchLeaf110 = icmp ule i32 %.off109, 1
  br i1 %SwitchLeaf110, label %switch_1_8641.i, label %NewDefault

NodeBlock106:                                     ; preds = %NodeBlock128
  %Pivot107 = icmp slt i32 %7, 8576
  br i1 %Pivot107, label %NodeBlock91, label %NodeBlock104

NodeBlock104:                                     ; preds = %NodeBlock106
  %Pivot105 = icmp slt i32 %7, 8592
  br i1 %Pivot105, label %LeafBlock93, label %NodeBlock102

NodeBlock102:                                     ; preds = %NodeBlock104
  %Pivot103 = icmp slt i32 %7, 8608
  br i1 %Pivot103, label %LeafBlock96, label %LeafBlock99

LeafBlock99:                                      ; preds = %NodeBlock102
  %.off100 = add i32 %7, -8608
  %SwitchLeaf101 = icmp ule i32 %.off100, 1
  br i1 %SwitchLeaf101, label %switch_1_8609.i, label %NewDefault

LeafBlock96:                                      ; preds = %NodeBlock102
  %.off97 = add i32 %7, -8592
  %SwitchLeaf98 = icmp ule i32 %.off97, 1
  br i1 %SwitchLeaf98, label %switch_1_8593.i, label %NewDefault

LeafBlock93:                                      ; preds = %NodeBlock104
  %.off94 = add i32 %7, -8576
  %SwitchLeaf95 = icmp ule i32 %.off94, 1
  br i1 %SwitchLeaf95, label %switch_1_8577.i, label %NewDefault

NodeBlock91:                                      ; preds = %NodeBlock106
  %Pivot92 = icmp slt i32 %7, 8560
  br i1 %Pivot92, label %LeafBlock85, label %LeafBlock88

LeafBlock88:                                      ; preds = %NodeBlock91
  %.off89 = add i32 %7, -8560
  %SwitchLeaf90 = icmp ule i32 %.off89, 1
  br i1 %SwitchLeaf90, label %switch_1_8561.i, label %NewDefault

LeafBlock85:                                      ; preds = %NodeBlock91
  %.off86 = add i32 %7, -8544
  %SwitchLeaf87 = icmp ule i32 %.off86, 1
  br i1 %SwitchLeaf87, label %switch_1_8545.i, label %NewDefault

NodeBlock83:                                      ; preds = %NodeBlock130
  %Pivot84 = icmp slt i32 %7, 8480
  br i1 %Pivot84, label %NodeBlock59, label %NodeBlock81

NodeBlock81:                                      ; preds = %NodeBlock83
  %Pivot82 = icmp slt i32 %7, 8496
  br i1 %Pivot82, label %NodeBlock66, label %NodeBlock79

NodeBlock79:                                      ; preds = %NodeBlock81
  %Pivot80 = icmp slt i32 %7, 8512
  br i1 %Pivot80, label %LeafBlock68, label %NodeBlock77

NodeBlock77:                                      ; preds = %NodeBlock79
  %Pivot78 = icmp slt i32 %7, 8528
  br i1 %Pivot78, label %LeafBlock71, label %LeafBlock74

LeafBlock74:                                      ; preds = %NodeBlock77
  %.off75 = add i32 %7, -8528
  %SwitchLeaf76 = icmp ule i32 %.off75, 1
  br i1 %SwitchLeaf76, label %switch_1_8529.i, label %NewDefault

LeafBlock71:                                      ; preds = %NodeBlock77
  %.off72 = add i32 %7, -8512
  %SwitchLeaf73 = icmp ule i32 %.off72, 1
  br i1 %SwitchLeaf73, label %switch_1_8513.i, label %NewDefault

LeafBlock68:                                      ; preds = %NodeBlock79
  %.off69 = add i32 %7, -8496
  %SwitchLeaf70 = icmp ule i32 %.off69, 1
  br i1 %SwitchLeaf70, label %switch_1_8497.i, label %NewDefault

NodeBlock66:                                      ; preds = %NodeBlock81
  %Pivot67 = icmp slt i32 %7, 8482
  br i1 %Pivot67, label %LeafBlock61, label %LeafBlock64

LeafBlock64:                                      ; preds = %NodeBlock66
  %SwitchLeaf65 = icmp eq i32 %7, 8482
  br i1 %SwitchLeaf65, label %switch_1_break.i, label %NewDefault

LeafBlock61:                                      ; preds = %NodeBlock66
  %.off62 = add i32 %7, -8480
  %SwitchLeaf63 = icmp ule i32 %.off62, 1
  br i1 %SwitchLeaf63, label %switch_1_8481.i, label %NewDefault

NodeBlock59:                                      ; preds = %NodeBlock83
  %Pivot60 = icmp slt i32 %7, 8448
  br i1 %Pivot60, label %NodeBlock, label %NodeBlock57

NodeBlock57:                                      ; preds = %NodeBlock59
  %Pivot58 = icmp slt i32 %7, 8464
  br i1 %Pivot58, label %LeafBlock53, label %LeafBlock55

LeafBlock55:                                      ; preds = %NodeBlock57
  %.off = add i32 %7, -8464
  %SwitchLeaf56 = icmp ule i32 %.off, 2
  br i1 %SwitchLeaf56, label %switch_1_8466.i, label %NewDefault

LeafBlock53:                                      ; preds = %NodeBlock57
  %SwitchLeaf54 = icmp eq i32 %7, 8448
  br i1 %SwitchLeaf54, label %if.then120.i, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock59
  %Pivot = icmp slt i32 %7, 8195
  br i1 %Pivot, label %LeafBlock, label %LeafBlock51

LeafBlock51:                                      ; preds = %NodeBlock
  %SwitchLeaf52 = icmp eq i32 %7, 8195
  br i1 %SwitchLeaf52, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 %7, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

if.then120.i:                                     ; preds = %LeafBlock53
  br label %switch_1_break.i

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock51, %LeafBlock53, %LeafBlock55, %LeafBlock61, %LeafBlock64, %LeafBlock68, %LeafBlock71, %LeafBlock74, %LeafBlock85, %LeafBlock88, %LeafBlock93, %LeafBlock96, %LeafBlock99, %LeafBlock108, %LeafBlock111, %LeafBlock116, %LeafBlock118, %LeafBlock120
  br label %if.else161.i

if.else161.i:                                     ; preds = %NewDefault
  br label %NodeBlock138

NodeBlock138:                                     ; preds = %if.else161.i
  %Pivot139 = icmp slt i32 %7, 8672
  br i1 %Pivot139, label %LeafBlock133, label %LeafBlock135

LeafBlock135:                                     ; preds = %NodeBlock138
  %.off136 = add i32 %7, -8672
  %SwitchLeaf137 = icmp ule i32 %.off136, 1
  br i1 %SwitchLeaf137, label %switch_1_8673.i, label %NewDefault132

LeafBlock133:                                     ; preds = %NodeBlock138
  %SwitchLeaf134 = icmp eq i32 %7, 3
  br i1 %SwitchLeaf134, label %ssl3_accept.exit, label %NewDefault132

switch_1_8481.i:                                  ; preds = %LeafBlock61
  %call203.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp204.i = icmp slt i32 %call203.i, 1
  br i1 %cmp204.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8466.i:                                  ; preds = %LeafBlock55
  %call208.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp209.i = icmp eq i32 %4, 0
  %8 = select i1 %cmp209.i, i32 1, i32 %4
  %cmp213.i = icmp slt i32 %call208.i, 1
  br i1 %cmp213.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8497.i:                                  ; preds = %LeafBlock68
  %call217.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp222.i = icmp slt i32 %call217.i, 1
  br i1 %cmp222.i, label %ssl3_accept.exit.loopexit, label %if.end225.i

if.end225.i:                                      ; preds = %switch_1_8497.i
  %cmp218.i = icmp eq i32 %4, 1
  %9 = select i1 %cmp218.i, i32 2, i32 %4
  br label %switch_1_break.i

switch_1_8513.i:                                  ; preds = %LeafBlock71
  %call230.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool232.i = icmp eq i32 %call230.i, -256
  br i1 %tobool232.i, label %if.else234.i, label %switch_1_break.i.thread

if.else234.i:                                     ; preds = %switch_1_8513.i
  %call235.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp236.i = icmp eq i32 %4, 2
  %10 = select i1 %cmp236.i, i32 3, i32 %4
  %cmp240.i = icmp slt i32 %call235.i, 1
  br i1 %cmp240.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8529.i:                                  ; preds = %LeafBlock74
  %call245.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool257.i = icmp eq i32 %call245.i, -30
  %or.cond = and i1 %tobool257.i, %tobool253.i
  br i1 %or.cond, label %if.then262.i, label %_L___0.i

if.then262.i:                                     ; preds = %switch_1_8529.i
  %cmp263.i = icmp eq i32 %5, 0
  br i1 %cmp263.i, label %_L___0.i, label %if.else266.i

if.else266.i:                                     ; preds = %if.then262.i
  %call267.i = tail call i32 (...)* @nondet_int() nounwind
  %conv268.i = sext i32 %call267.i to i64
  %tobool270.i = icmp eq i32 %call267.i, -2
  br i1 %tobool270.i, label %switch_1_break.i.thread, label %if.then271.i

if.then271.i:                                     ; preds = %if.else266.i
  %call272.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool275.i = icmp eq i32 %call272.i, -4
  %.7 = select i1 %tobool275.i, i32 1024, i32 512
  %cmp280.i = icmp sgt i32 %mul279.i, %.7
  br i1 %cmp280.i, label %_L___0.i, label %switch_1_break.i.thread

_L___0.i:                                         ; preds = %if.then271.i, %if.then262.i, %switch_1_8529.i
  %11 = phi i64 [ %conv268.i, %if.then271.i ], [ %3, %if.then262.i ], [ %3, %switch_1_8529.i ]
  %.731 = phi i32 [ %.7, %if.then271.i ], [ %.732, %if.then262.i ], [ %.732, %switch_1_8529.i ]
  %12 = phi i32 [ 100, %if.then271.i ], [ 0, %if.then262.i ], [ %5, %switch_1_8529.i ]
  %call283.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp284.i = icmp eq i32 %4, 3
  %13 = select i1 %cmp284.i, i32 4, i32 %4
  %cmp288.i = icmp slt i32 %call283.i, 1
  br i1 %cmp288.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8545.i:                                  ; preds = %LeafBlock85
  %tobool302.i = icmp eq i32 %6, -1
  br i1 %tobool302.i, label %switch_1_break.i.thread, label %if.then303.i

if.then303.i:                                     ; preds = %switch_1_8545.i
  %tobool308.i = icmp eq i32 %6, -4
  br i1 %tobool308.i, label %_L___2.i, label %switch_1_break.i.thread

_L___2.i:                                         ; preds = %if.then303.i
  %tobool314.i = icmp eq i64 %2, -256
  %14 = select i1 %tobool314.i, i64 -256, i64 9021
  %15 = select i1 %tobool314.i, i32 -4, i32 124
  %call322.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp323.i = icmp eq i32 %4, 4
  %16 = select i1 %cmp323.i, i32 5, i32 %4
  %cmp327.i = icmp slt i32 %call322.i, 1
  br i1 %cmp327.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8561.i:                                  ; preds = %LeafBlock88
  %call335.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp336.i = icmp slt i32 %call335.i, 1
  br i1 %cmp336.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8577.i:                                  ; preds = %LeafBlock93
  %call348.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp349.i = icmp eq i32 %4, 5
  %17 = select i1 %cmp349.i, i32 6, i32 %4
  %cmp353.i = icmp slt i32 %call348.i, 1
  br i1 %cmp353.i, label %ssl3_accept.exit.loopexit, label %if.end356.i

if.end356.i:                                      ; preds = %switch_1_8577.i
  %cmp357.i = icmp eq i32 %call348.i, 2
  br i1 %cmp357.i, label %switch_1_break.i, label %if.else360.i

if.else360.i:                                     ; preds = %if.end356.i
  %call361.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp362.i = icmp eq i32 %17, 6
  %18 = select i1 %cmp362.i, i32 7, i32 %17
  %cmp366.i = icmp slt i32 %call361.i, 1
  br i1 %cmp366.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8593.i:                                  ; preds = %LeafBlock96
  %call371.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp372.i = icmp eq i32 %4, 7
  %19 = select i1 %cmp372.i, i32 8, i32 %4
  %cmp376.i = icmp slt i32 %call371.i, 1
  br i1 %cmp376.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8609.i:                                  ; preds = %LeafBlock99
  %call380.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp381.i = icmp eq i32 %4, 8
  %20 = select i1 %cmp381.i, i32 9, i32 %4
  %cmp385.i = icmp slt i32 %call380.i, 1
  br i1 %cmp385.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8641.i:                                  ; preds = %LeafBlock108
  %call389.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp390.i = icmp eq i32 %4, 9
  br i1 %cmp390.i, label %if.end413.i, label %if.else393.i

if.else393.i:                                     ; preds = %switch_1_8641.i
  %cmp394.i = icmp eq i32 %4, 12
  br i1 %cmp394.i, label %if.end413.i, label %if.else397.i

if.else397.i:                                     ; preds = %if.else393.i
  %cmp398.i = icmp eq i32 %4, 15
  br i1 %cmp398.i, label %if.end413.i, label %if.else401.i

if.else401.i:                                     ; preds = %if.else397.i
  %cmp402.i = icmp eq i32 %4, 18
  br i1 %cmp402.i, label %if.end413.i, label %if.else405.i

if.else405.i:                                     ; preds = %if.else401.i
  %cmp406.i = icmp eq i32 %4, 21
  br i1 %cmp406.i, label %ERROR.i, label %if.end413.i

if.end413.i:                                      ; preds = %if.else405.i, %if.else401.i, %if.else397.i, %if.else393.i, %switch_1_8641.i
  %21 = phi i32 [ %4, %if.else405.i ], [ 10, %switch_1_8641.i ], [ 13, %if.else393.i ], [ 16, %if.else397.i ], [ 19, %if.else401.i ]
  %cmp414.i = icmp slt i32 %call389.i, 1
  br i1 %cmp414.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

if.end424.i:                                      ; preds = %LeafBlock111
  %call425.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp426.i = icmp eq i32 %4, 10
  br i1 %cmp426.i, label %if.end444.i, label %if.else429.i

if.else429.i:                                     ; preds = %if.end424.i
  %cmp430.i = icmp eq i32 %4, 13
  br i1 %cmp430.i, label %if.end444.i, label %if.else433.i

if.else433.i:                                     ; preds = %if.else429.i
  %cmp434.i = icmp eq i32 %4, 16
  br i1 %cmp434.i, label %if.end444.i, label %if.else437.i

if.else437.i:                                     ; preds = %if.else433.i
  %cmp438.i = icmp eq i32 %4, 19
  br i1 %cmp438.i, label %if.then440.i, label %if.end444.i

if.then440.i:                                     ; preds = %if.else437.i
  br label %if.end444.i

if.end444.i:                                      ; preds = %if.then440.i, %if.else437.i, %if.else433.i, %if.else429.i, %if.end424.i
  %22 = phi i32 [ %4, %if.else437.i ], [ 20, %if.then440.i ], [ 11, %if.end424.i ], [ 14, %if.else429.i ], [ 17, %if.else433.i ]
  %cmp445.i = icmp slt i32 %call425.i, 1
  br i1 %cmp445.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_8673.i:                                  ; preds = %LeafBlock135
  %call452.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp453.i = icmp eq i32 %4, 11
  br i1 %cmp453.i, label %if.end471.i, label %if.else456.i

if.else456.i:                                     ; preds = %switch_1_8673.i
  %cmp457.i = icmp eq i32 %4, 14
  br i1 %cmp457.i, label %if.end471.i, label %if.else460.i

if.else460.i:                                     ; preds = %if.else456.i
  %cmp461.i = icmp eq i32 %4, 17
  br i1 %cmp461.i, label %if.end471.i, label %if.else464.i

if.else464.i:                                     ; preds = %if.else460.i
  %cmp465.i = icmp eq i32 %4, 20
  br i1 %cmp465.i, label %if.then467.i, label %if.end471.i

if.then467.i:                                     ; preds = %if.else464.i
  br label %if.end471.i

if.end471.i:                                      ; preds = %if.then467.i, %if.else464.i, %if.else460.i, %if.else456.i, %switch_1_8673.i
  %23 = phi i32 [ %4, %if.else464.i ], [ 21, %if.then467.i ], [ 12, %switch_1_8673.i ], [ 15, %if.else456.i ], [ 18, %if.else460.i ]
  %cmp472.i = icmp slt i32 %call452.i, 1
  br i1 %cmp472.i, label %ssl3_accept.exit.loopexit, label %switch_1_break.i

switch_1_break.i.thread:                          ; preds = %if.then303.i, %switch_1_8545.i, %if.then271.i, %if.else266.i, %switch_1_8513.i
  %.ph42 = phi i64 [ %3, %switch_1_8513.i ], [ %conv268.i, %if.else266.i ], [ %conv268.i, %if.then271.i ], [ %3, %switch_1_8545.i ], [ %3, %if.then303.i ]
  %.729.ph = phi i32 [ %.732, %switch_1_8513.i ], [ %.732, %if.else266.i ], [ %.7, %if.then271.i ], [ %.732, %switch_1_8545.i ], [ %.732, %if.then303.i ]
  %.ph43 = phi i32 [ %5, %switch_1_8513.i ], [ 100, %if.else266.i ], [ 100, %if.then271.i ], [ %5, %switch_1_8545.i ], [ %5, %if.then303.i ]
  %.ph44 = phi i32 [ %6, %switch_1_8513.i ], [ %6, %if.else266.i ], [ %6, %if.then271.i ], [ -1, %switch_1_8545.i ], [ 123, %if.then303.i ]
  %.ph45 = phi i32 [ 8528, %switch_1_8513.i ], [ 8544, %if.else266.i ], [ 8544, %if.then271.i ], [ 8560, %switch_1_8545.i ], [ 8560, %if.then303.i ]
  %call521.i46 = tail call i32 (...)* @nondet_int() nounwind
  br label %while.body.i

switch_1_break.i:                                 ; preds = %if.end471.i, %if.end444.i, %if.end413.i, %switch_1_8609.i, %switch_1_8593.i, %if.else360.i, %if.end356.i, %switch_1_8561.i, %_L___2.i, %_L___0.i, %if.else234.i, %if.end225.i, %switch_1_8466.i, %switch_1_8481.i, %if.then120.i, %LeafBlock64
  %24 = phi i64 [ %2, %if.end225.i ], [ %2, %switch_1_8481.i ], [ %2, %switch_1_8466.i ], [ %2, %if.else234.i ], [ %2, %_L___0.i ], [ %14, %_L___2.i ], [ %2, %switch_1_8561.i ], [ %2, %if.then120.i ], [ %2, %if.else360.i ], [ %2, %if.end356.i ], [ %2, %switch_1_8593.i ], [ %2, %switch_1_8609.i ], [ %2, %if.end444.i ], [ %2, %LeafBlock64 ], [ %2, %if.end413.i ], [ %2, %if.end471.i ]
  %25 = phi i64 [ %3, %if.end225.i ], [ %3, %switch_1_8481.i ], [ %3, %switch_1_8466.i ], [ %3, %if.else234.i ], [ %11, %_L___0.i ], [ %3, %_L___2.i ], [ %3, %switch_1_8561.i ], [ %3, %if.then120.i ], [ %3, %if.else360.i ], [ %3, %if.end356.i ], [ %3, %switch_1_8593.i ], [ %3, %switch_1_8609.i ], [ %3, %if.end444.i ], [ %3, %LeafBlock64 ], [ %3, %if.end413.i ], [ %3, %if.end471.i ]
  %26 = phi i32 [ %9, %if.end225.i ], [ %4, %switch_1_8481.i ], [ %8, %switch_1_8466.i ], [ %10, %if.else234.i ], [ %13, %_L___0.i ], [ %16, %_L___2.i ], [ %4, %switch_1_8561.i ], [ %4, %if.then120.i ], [ %18, %if.else360.i ], [ %17, %if.end356.i ], [ %19, %switch_1_8593.i ], [ %20, %switch_1_8609.i ], [ %22, %if.end444.i ], [ %4, %LeafBlock64 ], [ %21, %if.end413.i ], [ %23, %if.end471.i ]
  %.729 = phi i32 [ %.732, %if.end225.i ], [ %.732, %switch_1_8481.i ], [ %.732, %switch_1_8466.i ], [ %.732, %if.else234.i ], [ %.731, %_L___0.i ], [ %.732, %_L___2.i ], [ %.732, %switch_1_8561.i ], [ %.732, %if.then120.i ], [ %.732, %if.else360.i ], [ %.732, %if.end356.i ], [ %.732, %switch_1_8593.i ], [ %.732, %switch_1_8609.i ], [ %.732, %if.end444.i ], [ %.732, %LeafBlock64 ], [ %.732, %if.end413.i ], [ %.732, %if.end471.i ]
  %.928 = phi i32 [ %.927, %if.end225.i ], [ 8482, %switch_1_8481.i ], [ %.927, %switch_1_8466.i ], [ %.927, %if.else234.i ], [ %.927, %_L___0.i ], [ 8576, %_L___2.i ], [ 8576, %switch_1_8561.i ], [ %.927, %if.then120.i ], [ %.927, %if.else360.i ], [ %.927, %if.end356.i ], [ %.927, %switch_1_8593.i ], [ %.927, %switch_1_8609.i ], [ %.927, %if.end444.i ], [ %.927, %LeafBlock64 ], [ %.927, %if.end413.i ], [ 8640, %if.end471.i ]
  %27 = phi i32 [ %5, %if.end225.i ], [ %5, %switch_1_8481.i ], [ %5, %switch_1_8466.i ], [ %5, %if.else234.i ], [ %12, %_L___0.i ], [ %5, %_L___2.i ], [ %5, %switch_1_8561.i ], [ %5, %if.then120.i ], [ %5, %if.else360.i ], [ %5, %if.end356.i ], [ %5, %switch_1_8593.i ], [ %5, %switch_1_8609.i ], [ %5, %if.end444.i ], [ %5, %LeafBlock64 ], [ %5, %if.end413.i ], [ %5, %if.end471.i ]
  %28 = phi i32 [ %6, %if.end225.i ], [ %6, %switch_1_8481.i ], [ %6, %switch_1_8466.i ], [ %6, %if.else234.i ], [ %6, %_L___0.i ], [ %15, %_L___2.i ], [ %6, %switch_1_8561.i ], [ %6, %if.then120.i ], [ %6, %if.else360.i ], [ %6, %if.end356.i ], [ %6, %switch_1_8593.i ], [ %6, %switch_1_8609.i ], [ %6, %if.end444.i ], [ %6, %LeafBlock64 ], [ %6, %if.end413.i ], [ %6, %if.end471.i ]
  %29 = phi i32 [ 8656, %if.end225.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8528, %if.else234.i ], [ 8544, %_L___0.i ], [ 8448, %_L___2.i ], [ 8448, %switch_1_8561.i ], [ %.927, %if.then120.i ], [ 8592, %if.else360.i ], [ 8466, %if.end356.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 8672, %if.end444.i ], [ 3, %LeafBlock64 ], [ 3, %if.end413.i ], [ 8448, %if.end471.i ]
  %call521.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool522.i = icmp eq i32 %call521.i, 0
  br i1 %tobool522.i, label %if.then525.i, label %while.body.i

if.then525.i:                                     ; preds = %switch_1_break.i
  %notlhs = icmp ne i32 %7, 8560
  %notrhs = icmp ne i32 %29, 8448
  %or.cond36.not = or i1 %notrhs, %notlhs
  %switch = icmp ugt i32 %28, -3
  %or.cond41 = or i1 %or.cond36.not, %switch
  br i1 %or.cond41, label %if.then559.i, label %if.then537.i

if.then537.i:                                     ; preds = %if.then525.i
  %cmp538.i = icmp eq i64 %24, 9021
  %cmp541.i = icmp eq i64 %25, 4294967294
  %or.cond37 = or i1 %cmp538.i, %cmp541.i
  %cmp544.i = icmp eq i32 %26, 4
  %or.cond38 = or i1 %or.cond37, %cmp544.i
  %cmp547.i = icmp eq i32 %.729, 1024
  %or.cond39 = or i1 %or.cond38, %cmp547.i
  br i1 %or.cond39, label %if.then559.i, label %ERROR.i

if.then559.i:                                     ; preds = %if.then537.i, %if.then525.i
  %call560.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp561.i = icmp slt i32 %call560.i, 1
  br i1 %cmp561.i, label %ssl3_accept.exit.loopexit, label %while.body.i

ERROR.i:                                          ; preds = %if.then537.i, %if.else405.i
  ret i32 5

NewDefault132:                                    ; preds = %LeafBlock133, %LeafBlock135
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault132, %if.then559.i, %if.end471.i, %if.end444.i, %if.end413.i, %switch_1_8609.i, %switch_1_8593.i, %if.else360.i, %switch_1_8577.i, %switch_1_8561.i, %_L___2.i, %_L___0.i, %if.else234.i, %switch_1_8497.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock, %LeafBlock51, %LeafBlock116, %LeafBlock118, %LeafBlock120
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock133, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i64 @nondet_2() readnone

declare i32 @exit(i32)
