; ModuleID = './tests/bench2/ssh-simplified/s3_srvr_11_BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
entry:
  %0 = tail call i32 @nondet_1() nounwind
  %1 = tail call i64 @nondet_2() nounwind
  %call.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp.i = icmp eq i32 %0, 0
  br i1 %cmp.i, label %ssl3_accept.exit, label %while.body.i.preheader

while.body.i.preheader:                           ; preds = %entry
  %tobool318.i = icmp eq i32 %0, -2
  %cmp476.i = icmp eq i32 %0, -12288
  %cmp479.i = icmp eq i32 %0, -16384
  %notrhs = icmp ne i32 %0, -2097152
  %mul282.i = shl nsw i32 %0, 3
  br label %while.body.i

NewDefault138:                                    ; preds = %LeafBlock139, %LeafBlock141
  br label %while.body.i

while.body.i:                                     ; preds = %LeafBlock146, %LeafBlock148, %if.then484.i, %if.then481.i, %if.then478.i, %if.then475.i, %if.end469.i, %if.end398.i, %if.end381.i, %switch_1_8609.i, %switch_1_8593.i, %if.end349.i, %switch_1_8561.i, %_L___1.i, %_L___2.i, %_L___0.i, %if.then274.i, %if.else269.i, %if.else241.i, %switch_1_8513.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock99, %LeafBlock61, %NewDefault138, %while.body.i.preheader
  %2 = phi i64 [ %1, %while.body.i.preheader ], [ %2, %if.then478.i ], [ 4294967294, %if.then484.i ], [ %2, %LeafBlock146 ], [ %2, %if.then481.i ], [ %2, %if.then475.i ], [ %2, %if.end469.i ], [ %2, %LeafBlock148 ], [ %2, %LeafBlock99 ], [ %2, %LeafBlock61 ], [ %conv271.i, %if.else269.i ], [ %2, %if.end381.i ], [ %2, %switch_1_8481.i ], [ %2, %switch_1_8466.i ], [ %2, %switch_1_8513.i ], [ %2, %if.else241.i ], [ %conv271.i, %if.then274.i ], [ %9, %_L___0.i ], [ %2, %_L___1.i ], [ %2, %switch_1_8561.i ], [ %2, %if.end349.i ], [ %2, %switch_1_8593.i ], [ %2, %switch_1_8609.i ], [ %2, %_L___2.i ], [ %2, %NewDefault138 ], [ %2, %if.end398.i ]
  %3 = phi i64 [ %1, %while.body.i.preheader ], [ %3, %if.then478.i ], [ %3, %if.then484.i ], [ %3, %LeafBlock146 ], [ 4294967040, %if.then481.i ], [ %3, %if.then475.i ], [ %3, %if.end469.i ], [ %3, %LeafBlock148 ], [ %3, %LeafBlock99 ], [ %3, %LeafBlock61 ], [ %3, %if.else269.i ], [ %3, %if.end381.i ], [ %3, %switch_1_8481.i ], [ %3, %switch_1_8466.i ], [ %conv238.i, %switch_1_8513.i ], [ %conv238.i, %if.else241.i ], [ %3, %if.then274.i ], [ %3, %_L___0.i ], [ %3, %_L___1.i ], [ %3, %switch_1_8561.i ], [ %3, %if.end349.i ], [ %3, %switch_1_8593.i ], [ %3, %switch_1_8609.i ], [ %3, %_L___2.i ], [ %3, %NewDefault138 ], [ %3, %if.end398.i ]
  %4 = phi i32 [ 0, %while.body.i.preheader ], [ %13, %if.then478.i ], [ %13, %if.then484.i ], [ %13, %LeafBlock146 ], [ %13, %if.then481.i ], [ %13, %if.then475.i ], [ %13, %if.end469.i ], [ %13, %LeafBlock148 ], [ %4, %LeafBlock99 ], [ %4, %LeafBlock61 ], [ %4, %if.else269.i ], [ %11, %if.end381.i ], [ %4, %switch_1_8481.i ], [ %7, %switch_1_8466.i ], [ %4, %switch_1_8513.i ], [ %4, %if.else241.i ], [ %4, %if.then274.i ], [ %4, %_L___0.i ], [ %4, %_L___1.i ], [ %4, %switch_1_8561.i ], [ %4, %if.end349.i ], [ %4, %switch_1_8593.i ], [ %4, %switch_1_8609.i ], [ %4, %_L___2.i ], [ %4, %NewDefault138 ], [ %4, %if.end398.i ]
  %5 = phi i32 [ %0, %while.body.i.preheader ], [ %5, %if.then478.i ], [ %5, %if.then484.i ], [ %5, %LeafBlock146 ], [ %5, %if.then481.i ], [ %5, %if.then475.i ], [ %5, %if.end469.i ], [ %5, %LeafBlock148 ], [ %5, %LeafBlock99 ], [ %5, %LeafBlock61 ], [ %5, %if.else269.i ], [ %5, %if.end381.i ], [ %5, %switch_1_8481.i ], [ %5, %switch_1_8466.i ], [ %5, %switch_1_8513.i ], [ %5, %if.else241.i ], [ %.7, %if.then274.i ], [ %10, %_L___0.i ], [ %5, %_L___1.i ], [ %5, %switch_1_8561.i ], [ %5, %if.end349.i ], [ %5, %switch_1_8593.i ], [ %5, %switch_1_8609.i ], [ %5, %_L___2.i ], [ %5, %NewDefault138 ], [ %5, %if.end398.i ]
  %.927 = phi i32 [ %0, %while.body.i.preheader ], [ %.927, %if.then478.i ], [ %.927, %if.then484.i ], [ %.927, %LeafBlock146 ], [ %.927, %if.then481.i ], [ %.927, %if.then475.i ], [ %.927, %if.end469.i ], [ %.927, %LeafBlock148 ], [ %.927, %LeafBlock99 ], [ %.927, %LeafBlock61 ], [ %.927, %if.else269.i ], [ %.927, %if.end381.i ], [ 8482, %switch_1_8481.i ], [ %.927, %switch_1_8466.i ], [ %.927, %switch_1_8513.i ], [ %.927, %if.else241.i ], [ %.927, %if.then274.i ], [ %.927, %_L___0.i ], [ 8576, %_L___1.i ], [ 8576, %switch_1_8561.i ], [ %.927, %if.end349.i ], [ %.927, %switch_1_8593.i ], [ %.927, %switch_1_8609.i ], [ %.927, %_L___2.i ], [ %.927, %NewDefault138 ], [ 8640, %if.end398.i ]
  %6 = phi i32 [ 8464, %while.body.i.preheader ], [ %12, %if.then478.i ], [ %12, %if.then484.i ], [ %12, %LeafBlock146 ], [ %12, %if.then481.i ], [ 8592, %if.then475.i ], [ %12, %if.end469.i ], [ %12, %LeafBlock148 ], [ %.927, %LeafBlock99 ], [ 3, %LeafBlock61 ], [ 8544, %if.else269.i ], [ 8672, %if.end381.i ], [ 8448, %switch_1_8481.i ], [ 8496, %switch_1_8466.i ], [ 8528, %switch_1_8513.i ], [ 8528, %if.else241.i ], [ 8544, %if.then274.i ], [ 8544, %_L___0.i ], [ 8448, %_L___1.i ], [ 8448, %switch_1_8561.i ], [ 8466, %if.end349.i ], [ 8608, %switch_1_8593.i ], [ 8640, %switch_1_8609.i ], [ 8560, %_L___2.i ], [ 8560, %NewDefault138 ], [ 8448, %if.end398.i ]
  br label %NodeBlock94

NodeBlock94:                                      ; preds = %while.body.i
  %Pivot95 = icmp slt i32 %6, 8496
  br i1 %Pivot95, label %NodeBlock67, label %NodeBlock92

NodeBlock92:                                      ; preds = %NodeBlock94
  %Pivot93 = icmp slt i32 %6, 12292
  br i1 %Pivot93, label %NodeBlock80, label %NodeBlock90

NodeBlock90:                                      ; preds = %NodeBlock92
  %Pivot91 = icmp slt i32 %6, 16384
  br i1 %Pivot91, label %LeafBlock82, label %NodeBlock88

NodeBlock88:                                      ; preds = %NodeBlock90
  %Pivot89 = icmp slt i32 %6, 24576
  br i1 %Pivot89, label %LeafBlock84, label %LeafBlock86

LeafBlock86:                                      ; preds = %NodeBlock88
  %SwitchLeaf87 = icmp eq i32 %6, 24576
  br i1 %SwitchLeaf87, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock84:                                      ; preds = %NodeBlock88
  %SwitchLeaf85 = icmp eq i32 %6, 16384
  br i1 %SwitchLeaf85, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock82:                                      ; preds = %NodeBlock90
  %SwitchLeaf83 = icmp eq i32 %6, 12292
  br i1 %SwitchLeaf83, label %ssl3_accept.exit.loopexit, label %NewDefault

NodeBlock80:                                      ; preds = %NodeBlock92
  %Pivot81 = icmp slt i32 %6, 8512
  br i1 %Pivot81, label %LeafBlock69, label %NodeBlock78

NodeBlock78:                                      ; preds = %NodeBlock80
  %Pivot79 = icmp slt i32 %6, 8528
  br i1 %Pivot79, label %LeafBlock72, label %LeafBlock75

LeafBlock75:                                      ; preds = %NodeBlock78
  %.off76 = add i32 %6, -8528
  %SwitchLeaf77 = icmp ule i32 %.off76, 1
  br i1 %SwitchLeaf77, label %switch_1_8529.i, label %NewDefault

LeafBlock72:                                      ; preds = %NodeBlock78
  %.off73 = add i32 %6, -8512
  %SwitchLeaf74 = icmp ule i32 %.off73, 1
  br i1 %SwitchLeaf74, label %switch_1_8513.i, label %NewDefault

LeafBlock69:                                      ; preds = %NodeBlock80
  %.off70 = add i32 %6, -8496
  %SwitchLeaf71 = icmp ule i32 %.off70, 1
  br i1 %SwitchLeaf71, label %switch_1_8497.i, label %NewDefault

NodeBlock67:                                      ; preds = %NodeBlock94
  %Pivot68 = icmp slt i32 %6, 8464
  br i1 %Pivot68, label %NodeBlock, label %NodeBlock65

NodeBlock65:                                      ; preds = %NodeBlock67
  %Pivot66 = icmp slt i32 %6, 8480
  br i1 %Pivot66, label %LeafBlock55, label %NodeBlock63

NodeBlock63:                                      ; preds = %NodeBlock65
  %Pivot64 = icmp slt i32 %6, 8482
  br i1 %Pivot64, label %LeafBlock58, label %LeafBlock61

LeafBlock61:                                      ; preds = %NodeBlock63
  %SwitchLeaf62 = icmp eq i32 %6, 8482
  br i1 %SwitchLeaf62, label %while.body.i, label %NewDefault

LeafBlock58:                                      ; preds = %NodeBlock63
  %.off59 = add i32 %6, -8480
  %SwitchLeaf60 = icmp ule i32 %.off59, 1
  br i1 %SwitchLeaf60, label %switch_1_8481.i, label %NewDefault

LeafBlock55:                                      ; preds = %NodeBlock65
  %.off56 = add i32 %6, -8464
  %SwitchLeaf57 = icmp ule i32 %.off56, 2
  br i1 %SwitchLeaf57, label %switch_1_8466.i, label %NewDefault

NodeBlock:                                        ; preds = %NodeBlock67
  %Pivot = icmp slt i32 %6, 8195
  br i1 %Pivot, label %LeafBlock, label %LeafBlock53

LeafBlock53:                                      ; preds = %NodeBlock
  %SwitchLeaf54 = icmp eq i32 %6, 8195
  br i1 %SwitchLeaf54, label %ssl3_accept.exit.loopexit, label %NewDefault

LeafBlock:                                        ; preds = %NodeBlock
  %SwitchLeaf = icmp eq i32 %6, 8192
  br i1 %SwitchLeaf, label %ssl3_accept.exit.loopexit, label %NewDefault

NewDefault:                                       ; preds = %LeafBlock, %LeafBlock53, %LeafBlock55, %LeafBlock58, %LeafBlock61, %LeafBlock69, %LeafBlock72, %LeafBlock75, %LeafBlock82, %LeafBlock84, %LeafBlock86
  br label %if.else103.i

if.else103.i:                                     ; preds = %NewDefault
  %.off = add i32 %6, -8544
  %switch = icmp ult i32 %.off, 2
  br i1 %switch, label %switch_1_8545.i, label %if.else111.i

if.else111.i:                                     ; preds = %if.else103.i
  br label %NodeBlock136

NodeBlock136:                                     ; preds = %if.else111.i
  %Pivot137 = icmp slt i32 %6, 8592
  br i1 %Pivot137, label %NodeBlock111, label %NodeBlock134

NodeBlock134:                                     ; preds = %NodeBlock136
  %Pivot135 = icmp slt i32 %6, 8640
  br i1 %Pivot135, label %NodeBlock119, label %NodeBlock132

NodeBlock132:                                     ; preds = %NodeBlock134
  %Pivot133 = icmp slt i32 %6, 8656
  br i1 %Pivot133, label %LeafBlock121, label %NodeBlock130

NodeBlock130:                                     ; preds = %NodeBlock132
  %Pivot131 = icmp slt i32 %6, 8672
  br i1 %Pivot131, label %LeafBlock124, label %LeafBlock127

LeafBlock127:                                     ; preds = %NodeBlock130
  %.off128 = add i32 %6, -8672
  %SwitchLeaf129 = icmp ule i32 %.off128, 1
  br i1 %SwitchLeaf129, label %switch_1_8673.i, label %NewDefault96

LeafBlock124:                                     ; preds = %NodeBlock130
  %.off125 = add i32 %6, -8656
  %SwitchLeaf126 = icmp ule i32 %.off125, 1
  br i1 %SwitchLeaf126, label %switch_1_8657.i, label %NewDefault96

LeafBlock121:                                     ; preds = %NodeBlock132
  %.off122 = add i32 %6, -8640
  %SwitchLeaf123 = icmp ule i32 %.off122, 1
  br i1 %SwitchLeaf123, label %switch_1_8641.i, label %NewDefault96

NodeBlock119:                                     ; preds = %NodeBlock134
  %Pivot120 = icmp slt i32 %6, 8608
  br i1 %Pivot120, label %LeafBlock113, label %LeafBlock116

LeafBlock116:                                     ; preds = %NodeBlock119
  %.off117 = add i32 %6, -8608
  %SwitchLeaf118 = icmp ule i32 %.off117, 1
  br i1 %SwitchLeaf118, label %switch_1_8609.i, label %NewDefault96

LeafBlock113:                                     ; preds = %NodeBlock119
  %.off114 = add i32 %6, -8592
  %SwitchLeaf115 = icmp ule i32 %.off114, 1
  br i1 %SwitchLeaf115, label %switch_1_8593.i, label %NewDefault96

NodeBlock111:                                     ; preds = %NodeBlock136
  %Pivot112 = icmp slt i32 %6, 8560
  br i1 %Pivot112, label %NodeBlock101, label %NodeBlock109

NodeBlock109:                                     ; preds = %NodeBlock111
  %Pivot110 = icmp slt i32 %6, 8576
  br i1 %Pivot110, label %LeafBlock103, label %LeafBlock106

LeafBlock106:                                     ; preds = %NodeBlock109
  %.off107 = add i32 %6, -8576
  %SwitchLeaf108 = icmp ule i32 %.off107, 1
  br i1 %SwitchLeaf108, label %switch_1_8577.i, label %NewDefault96

LeafBlock103:                                     ; preds = %NodeBlock109
  %.off104 = add i32 %6, -8560
  %SwitchLeaf105 = icmp ule i32 %.off104, 1
  br i1 %SwitchLeaf105, label %switch_1_8561.i, label %NewDefault96

NodeBlock101:                                     ; preds = %NodeBlock111
  %Pivot102 = icmp slt i32 %6, 8448
  br i1 %Pivot102, label %LeafBlock97, label %LeafBlock99

LeafBlock99:                                      ; preds = %NodeBlock101
  %SwitchLeaf100 = icmp eq i32 %6, 8448
  br i1 %SwitchLeaf100, label %while.body.i, label %NewDefault96

LeafBlock97:                                      ; preds = %NodeBlock101
  %SwitchLeaf98 = icmp eq i32 %6, 3
  br i1 %SwitchLeaf98, label %ssl3_accept.exit, label %NewDefault96

switch_1_8481.i:                                  ; preds = %LeafBlock58
  %call205.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp206.i = icmp slt i32 %call205.i, 1
  br i1 %cmp206.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8466.i:                                  ; preds = %LeafBlock55
  %call210.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp211.i = icmp eq i32 %4, 0
  %7 = select i1 %cmp211.i, i32 1, i32 %4
  %cmp215.i = icmp slt i32 %call210.i, 1
  br i1 %cmp215.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8497.i:                                  ; preds = %LeafBlock69
  %call219.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp220.i = icmp eq i32 %4, 1
  br i1 %cmp220.i, label %if.end228.i, label %if.else223.i

if.else223.i:                                     ; preds = %switch_1_8497.i
  %cmp224.i = icmp eq i32 %4, 3
  br i1 %cmp224.i, label %if.then226.i, label %if.end228.i

if.then226.i:                                     ; preds = %if.else223.i
  br label %if.end228.i

if.end228.i:                                      ; preds = %if.then226.i, %if.else223.i, %switch_1_8497.i
  %8 = phi i32 [ %4, %if.else223.i ], [ 4, %if.then226.i ], [ 2, %switch_1_8497.i ]
  %cmp229.i = icmp slt i32 %call219.i, 1
  br i1 %cmp229.i, label %ssl3_accept.exit.loopexit, label %if.end469.i

switch_1_8513.i:                                  ; preds = %LeafBlock72
  %call237.i = tail call i32 (...)* @nondet_int() nounwind
  %conv238.i = sext i32 %call237.i to i64
  %tobool239.i = icmp eq i32 %call237.i, -256
  br i1 %tobool239.i, label %if.else241.i, label %while.body.i

if.else241.i:                                     ; preds = %switch_1_8513.i
  %call242.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp243.i = icmp slt i32 %call242.i, 1
  br i1 %cmp243.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8529.i:                                  ; preds = %LeafBlock75
  %call248.i = tail call i32 (...)* @nondet_int() nounwind
  %notlhs = icmp ne i32 %call248.i, -30
  %or.cond.not = or i1 %notrhs, %notlhs
  %brmerge = or i1 %or.cond.not, %cmp.i
  br i1 %brmerge, label %_L___0.i, label %if.else269.i

if.else269.i:                                     ; preds = %switch_1_8529.i
  %call270.i = tail call i32 (...)* @nondet_int() nounwind
  %conv271.i = sext i32 %call270.i to i64
  %tobool273.i = icmp eq i32 %call270.i, -2
  br i1 %tobool273.i, label %while.body.i, label %if.then274.i

if.then274.i:                                     ; preds = %if.else269.i
  %call275.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool278.i = icmp eq i32 %call275.i, -4
  %.7 = select i1 %tobool278.i, i32 1024, i32 512
  %cmp283.i = icmp sgt i32 %mul282.i, %.7
  br i1 %cmp283.i, label %_L___0.i, label %while.body.i

_L___0.i:                                         ; preds = %if.then274.i, %switch_1_8529.i
  %9 = phi i64 [ %conv271.i, %if.then274.i ], [ %2, %switch_1_8529.i ]
  %10 = phi i32 [ %.7, %if.then274.i ], [ %5, %switch_1_8529.i ]
  %call286.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp287.i = icmp slt i32 %call286.i, 1
  br i1 %cmp287.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8545.i:                                  ; preds = %if.else103.i
  br label %NodeBlock143

NodeBlock143:                                     ; preds = %switch_1_8545.i
  %Pivot144 = icmp slt i32 %0, 0
  br i1 %Pivot144, label %LeafBlock139, label %LeafBlock141

LeafBlock141:                                     ; preds = %NodeBlock143
  %SwitchLeaf142 = icmp eq i32 %0, 0
  br i1 %SwitchLeaf142, label %_L___2.i, label %NewDefault138

LeafBlock139:                                     ; preds = %NodeBlock143
  %SwitchLeaf140 = icmp eq i32 %0, -4
  br i1 %SwitchLeaf140, label %_L___2.i, label %NewDefault138

_L___2.i:                                         ; preds = %LeafBlock139, %LeafBlock141
  %call312.i = tail call i32 (...)* @nondet_int() nounwind
  %tobool315.i = icmp ne i32 %call312.i, -256
  %or.cond35 = and i1 %tobool315.i, %tobool318.i
  br i1 %or.cond35, label %while.body.i, label %_L___1.i

_L___1.i:                                         ; preds = %_L___2.i
  %call323.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp324.i = icmp slt i32 %call323.i, 1
  br i1 %cmp324.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8561.i:                                  ; preds = %LeafBlock103
  %call332.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp333.i = icmp slt i32 %call332.i, 1
  br i1 %cmp333.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8577.i:                                  ; preds = %LeafBlock106
  %call345.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp346.i = icmp slt i32 %call345.i, 1
  br i1 %cmp346.i, label %ssl3_accept.exit.loopexit, label %if.end349.i

if.end349.i:                                      ; preds = %switch_1_8577.i
  %cmp350.i = icmp eq i32 %call345.i, 2
  br i1 %cmp350.i, label %while.body.i, label %if.else353.i

if.else353.i:                                     ; preds = %if.end349.i
  %call354.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp355.i = icmp slt i32 %call354.i, 1
  br i1 %cmp355.i, label %ssl3_accept.exit.loopexit, label %if.end469.i

switch_1_8593.i:                                  ; preds = %LeafBlock113
  %call360.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp361.i = icmp slt i32 %call360.i, 1
  br i1 %cmp361.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8609.i:                                  ; preds = %LeafBlock116
  %call365.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp366.i = icmp slt i32 %call365.i, 1
  br i1 %cmp366.i, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8641.i:                                  ; preds = %LeafBlock121
  %call370.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp371.i = icmp slt i32 %call370.i, 1
  br i1 %cmp371.i, label %ssl3_accept.exit.loopexit, label %if.end469.i

switch_1_8657.i:                                  ; preds = %LeafBlock124
  br i1 %cmp.i, label %ssl3_accept.exit.loopexit, label %if.end381.i

if.end381.i:                                      ; preds = %switch_1_8657.i
  %call382.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp383.i = icmp eq i32 %4, 2
  %11 = select i1 %cmp383.i, i32 3, i32 %4
  %cmp387.i = icmp slt i32 %call382.i, 1
  %brmerge36 = or i1 %cmp387.i, %cmp.i
  br i1 %brmerge36, label %ssl3_accept.exit.loopexit, label %while.body.i

switch_1_8673.i:                                  ; preds = %LeafBlock127
  %call394.i = tail call i32 (...)* @nondet_int() nounwind
  %cmp395.i = icmp eq i32 %4, 4
  br i1 %cmp395.i, label %ERROR.i, label %if.end398.i

if.end398.i:                                      ; preds = %switch_1_8673.i
  %cmp399.i = icmp slt i32 %call394.i, 1
  br i1 %cmp399.i, label %ssl3_accept.exit.loopexit, label %while.body.i

if.end469.i:                                      ; preds = %switch_1_8641.i, %if.else353.i, %if.end228.i
  %12 = phi i32 [ 8592, %if.else353.i ], [ 8656, %if.end228.i ], [ 3, %switch_1_8641.i ]
  %13 = phi i32 [ %4, %if.else353.i ], [ %8, %if.end228.i ], [ %4, %switch_1_8641.i ]
  %cmp470.i = icmp eq i32 %6, 8576
  %cmp473.i = icmp eq i32 %12, 8592
  %or.cond38 = and i1 %cmp470.i, %cmp473.i
  br i1 %or.cond38, label %if.then475.i, label %while.body.i

if.then475.i:                                     ; preds = %if.end469.i
  br i1 %cmp476.i, label %while.body.i, label %if.then478.i

if.then478.i:                                     ; preds = %if.then475.i
  br i1 %cmp479.i, label %while.body.i, label %if.then481.i

if.then481.i:                                     ; preds = %if.then478.i
  %cmp482.i = icmp eq i64 %3, 4294967040
  br i1 %cmp482.i, label %while.body.i, label %if.then484.i

if.then484.i:                                     ; preds = %if.then481.i
  %cmp485.i = icmp eq i64 %2, 4294967294
  br i1 %cmp485.i, label %while.body.i, label %if.then487.i

if.then487.i:                                     ; preds = %if.then484.i
  br label %NodeBlock150

NodeBlock150:                                     ; preds = %if.then487.i
  %Pivot151 = icmp slt i32 %5, 1024
  br i1 %Pivot151, label %LeafBlock146, label %LeafBlock148

LeafBlock148:                                     ; preds = %NodeBlock150
  %SwitchLeaf149 = icmp eq i32 %5, 1024
  br i1 %SwitchLeaf149, label %while.body.i, label %NewDefault145

LeafBlock146:                                     ; preds = %NodeBlock150
  %SwitchLeaf147 = icmp eq i32 %5, 512
  br i1 %SwitchLeaf147, label %while.body.i, label %NewDefault145

NewDefault145:                                    ; preds = %LeafBlock146, %LeafBlock148
  br label %ERROR.i

ERROR.i:                                          ; preds = %NewDefault145, %switch_1_8673.i
  ret i32 5

NewDefault96:                                     ; preds = %LeafBlock97, %LeafBlock99, %LeafBlock103, %LeafBlock106, %LeafBlock113, %LeafBlock116, %LeafBlock121, %LeafBlock124, %LeafBlock127
  br label %ssl3_accept.exit.loopexit

ssl3_accept.exit.loopexit:                        ; preds = %NewDefault96, %if.end398.i, %if.end381.i, %switch_1_8657.i, %switch_1_8641.i, %switch_1_8609.i, %switch_1_8593.i, %if.else353.i, %switch_1_8577.i, %switch_1_8561.i, %_L___1.i, %_L___0.i, %if.else241.i, %if.end228.i, %switch_1_8466.i, %switch_1_8481.i, %LeafBlock, %LeafBlock53, %LeafBlock82, %LeafBlock84, %LeafBlock86
  br label %ssl3_accept.exit

ssl3_accept.exit:                                 ; preds = %ssl3_accept.exit.loopexit, %LeafBlock97, %entry
  br label %_UFO__exit

_UFO__exit:                                       ; preds = %_UFO__exit, %ssl3_accept.exit
  br label %_UFO__exit
}

declare i32 @nondet_1() readnone

declare i64 @nondet_2() readnone

declare i32 @exit(i32)
