/* Generated by CIL v. 1.3.7 */
/* print_CIL_Input is true */

#line 8 "structure_assignment.c"
struct Stuff {
   int a ;
   int b ;
};
#line 8 "structure_assignment.c"
typedef struct Stuff Stuff;
#line 69 "/usr/include/assert.h"
extern  __attribute__((__nothrow__, __noreturn__)) void __assert_fail(char const   *__assertion ,
                                                                      char const   *__file ,
                                                                      unsigned int __line ,
                                                                      char const   *__function ) ;
#line 6 "structure_assignment.c"
int VERDICT_SAFE  ;
#line 13 "structure_assignment.c"
int main(void) 
{ Stuff good ;
  Stuff bad ;

  {
#line 15
  good.a = 1;
#line 15
  good.b = 2;
#line 17
  bad = good;
#line 18
  if (bad.b == 2) {

  } else {
    {
#line 18
    __assert_fail("bad.b == 2", "structure_assignment.c", 18U, "main");
    }
  }
#line 19
  return (0);
}
}
