; ModuleID = 'tests/bench2/systemc/transmitter.01.BUG.cil.co.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

declare i32 @nondet_int(...)

define i32 @main() nounwind uwtable {
if.end3.i24.i:
  %call = tail call i32 (...)* @nondet_int() nounwind, !dbg !43
  %call1 = tail call i32 (...)* @nondet_int() nounwind, !dbg !46
  %call2 = tail call i32 (...)* @nondet_int() nounwind, !dbg !47
  %call3 = tail call i32 (...)* @nondet_int() nounwind, !dbg !48
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !49), !dbg !51
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !49), !dbg !51
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !54), !dbg !56
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !54), !dbg !62
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !54), !dbg !65
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !66), !dbg !58
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !66), !dbg !58
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !66), !dbg !67
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !68), !dbg !70
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !68), !dbg !73
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !68), !dbg !76
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !77), !dbg !72
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !77), !dbg !72
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !77), !dbg !78
  br label %while.body.i.i.outer, !dbg !79

while.body.i.i.outer:                             ; preds = %if.then.i, %eval.exit.i, %if.then13.i.i, %if.end3.i24.i
  %.ph = phi i32 [ %2, %if.then13.i.i ], [ 0, %if.end3.i24.i ], [ %.lcssa, %if.then.i ], [ %.lcssa, %eval.exit.i ]
  %.ph71 = phi i32 [ 1, %if.then13.i.i ], [ 0, %if.end3.i24.i ], [ %.ph71, %if.then.i ], [ %.ph71, %eval.exit.i ]
  %.26.ph = phi i32 [ %.2238, %if.then13.i.i ], [ 0, %if.end3.i24.i ], [ 0, %if.then.i ], [ 0, %eval.exit.i ]
  %storemerge120.ph = phi i32 [ 2, %if.then13.i.i ], [ 0, %if.end3.i24.i ], [ %storemerge120.ph77, %if.then.i ], [ %storemerge120.ph77, %eval.exit.i ]
  %cmp.i2.i.i.i.i.i = icmp eq i32 %.ph71, 1, !dbg !83
  br label %while.body.i.i.outer73

while.body.i.i.outer73:                           ; preds = %if.then10.i.i, %while.body.i.i.outer
  %.ph74 = phi i32 [ %2, %if.then10.i.i ], [ %.ph, %while.body.i.i.outer ]
  %.26.ph76 = phi i32 [ %.2238, %if.then10.i.i ], [ %.26.ph, %while.body.i.i.outer ]
  %storemerge120.ph77 = phi i32 [ 0, %if.then10.i.i ], [ %storemerge120.ph, %while.body.i.i.outer ]
  %cmp9.i.i = icmp eq i32 %storemerge120.ph77, 0, !dbg !100
  br i1 %cmp9.i.i, label %while.body.i.i.outer73.split.us, label %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge

while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge: ; preds = %while.body.i.i.outer73
  br i1 %cmp.i2.i.i.i.i.i, label %while.body.i.i.us84, label %while.body.i.i

while.body.i.i.outer73.split.us:                  ; preds = %while.body.i.i.outer73
  %cmp.i.i28.i.us.us = icmp eq i32 %.26.ph76, 0, !dbg !101
  br i1 %cmp.i2.i.i.i.i.i, label %while.body.i.i.outer73.split.us.split.us, label %while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge

while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge: ; preds = %while.body.i.i.outer73.split.us
  br i1 %cmp.i.i28.i.us.us, label %if.then3.i.i.us, label %if.then10.i.i, !dbg !101

while.body.i.i.outer73.split.us.split.us:         ; preds = %while.body.i.i.outer73.split.us
  br i1 %cmp.i.i28.i.us.us, label %if.then3.i.i.us.us, label %if.else.i.i31.i, !dbg !101

if.then3.i.i.us.us:                               ; preds = %while.body.i.i.outer73.split.us.split.us
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !106
  %call4.i.i.us.us = tail call i32 (...)* @nondet_int() nounwind, !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us.us}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us.us}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us.us}, i64 0, metadata !108), !dbg !109
  %tobool5.i.i.us.us = icmp eq i32 %call4.i.i.us.us, 0, !dbg !109
  br i1 %tobool5.i.i.us.us, label %if.then10.i.i, label %if.then6.i.i.us.us, !dbg !109

if.then6.i.i.us.us:                               ; preds = %if.then3.i.i.us.us
  br label %if.then10.i.i, !dbg !110

if.then3.i.i.us:                                  ; preds = %while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !106
  %call4.i.i.us = tail call i32 (...)* @nondet_int() nounwind, !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us}, i64 0, metadata !108), !dbg !109
  %tobool5.i.i.us = icmp eq i32 %call4.i.i.us, 0, !dbg !109
  br i1 %tobool5.i.i.us, label %if.then10.i.i, label %if.then6.i.i.us, !dbg !109

if.then6.i.i.us:                                  ; preds = %if.then3.i.i.us
  br label %if.then10.i.i, !dbg !110

while.body.i.i.us84:                              ; preds = %if.then6.i.i.us92, %if.then3.i.i.us89, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge
  %0 = phi i32 [ %.ph74, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge ], [ %0, %if.then3.i.i.us89 ], [ 1, %if.then6.i.i.us92 ]
  %.26.us85 = phi i32 [ %.26.ph76, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge ], [ 0, %if.then3.i.i.us89 ], [ 2, %if.then6.i.i.us92 ]
  %cmp.i.i28.i.us86 = icmp eq i32 %.26.us85, 0, !dbg !101
  br i1 %cmp.i.i28.i.us86, label %if.then3.i.i.us89, label %if.else.i.i31.i, !dbg !101

if.then3.i.i.us89:                                ; preds = %while.body.i.i.us84
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !106
  %call4.i.i.us90 = tail call i32 (...)* @nondet_int() nounwind, !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us90}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us90}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i.us90}, i64 0, metadata !108), !dbg !109
  %tobool5.i.i.us91 = icmp eq i32 %call4.i.i.us90, 0, !dbg !109
  br i1 %tobool5.i.i.us91, label %while.body.i.i.us84, label %if.then6.i.i.us92, !dbg !109

if.then6.i.i.us92:                                ; preds = %if.then3.i.i.us89
  %cmp1.i2.i.i.us93 = icmp eq i32 %0, 1, !dbg !111
  br i1 %cmp1.i2.i.i.us93, label %while.body.i.i.us84, label %if.then10.i.i, !dbg !110

while.body.i.i:                                   ; preds = %if.then6.i.i, %if.then3.i.i, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge
  %1 = phi i32 [ %.ph74, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge ], [ %1, %if.then3.i.i ], [ 1, %if.then6.i.i ]
  %.26 = phi i32 [ %.26.ph76, %while.body.i.i.outer73.while.body.i.i.outer73.split_crit_edge ], [ 0, %if.then3.i.i ], [ 2, %if.then6.i.i ]
  %cmp.i.i28.i = icmp eq i32 %.26, 0, !dbg !101
  br i1 %cmp.i.i28.i, label %if.then3.i.i, label %if.else.i.i31.i, !dbg !101

if.else.i.i31.i:                                  ; preds = %while.body.i.i, %while.body.i.i.us84, %while.body.i.i.outer73.split.us.split.us
  %.26.lcssa = phi i32 [ %.26.ph76, %while.body.i.i.outer73.split.us.split.us ], [ %.26.us85, %while.body.i.i.us84 ], [ %.26, %while.body.i.i ]
  %.lcssa = phi i32 [ %.ph74, %while.body.i.i.outer73.split.us.split.us ], [ %0, %while.body.i.i.us84 ], [ %1, %while.body.i.i ]
  br i1 %cmp9.i.i, label %if.then10.i.i, label %eval.exit.i, !dbg !113

if.then3.i.i:                                     ; preds = %while.body.i.i
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !106
  %call4.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i}, i64 0, metadata !108), !dbg !107
  tail call void @llvm.dbg.value(metadata !{i32 %call4.i.i}, i64 0, metadata !108), !dbg !109
  %tobool5.i.i = icmp eq i32 %call4.i.i, 0, !dbg !109
  br i1 %tobool5.i.i, label %while.body.i.i, label %if.then6.i.i, !dbg !109

if.then6.i.i:                                     ; preds = %if.then3.i.i
  br label %while.body.i.i, !dbg !110

if.then10.i.i:                                    ; preds = %if.else.i.i31.i, %if.then6.i.i.us92, %if.then6.i.i.us, %if.then3.i.i.us, %if.then6.i.i.us.us, %if.then3.i.i.us.us, %while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge
  %.2238 = phi i32 [ %.26.lcssa, %if.else.i.i31.i ], [ 0, %if.then3.i.i.us.us ], [ 2, %if.then6.i.i.us.us ], [ 0, %if.then3.i.i.us ], [ 2, %if.then6.i.i.us ], [ 2, %if.then6.i.i.us92 ], [ %.26.ph76, %while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge ]
  %2 = phi i32 [ %.lcssa, %if.else.i.i31.i ], [ %.ph74, %if.then3.i.i.us.us ], [ 1, %if.then6.i.i.us.us ], [ %.ph74, %if.then3.i.i.us ], [ 1, %if.then6.i.i.us ], [ 1, %if.then6.i.i.us92 ], [ %.ph74, %while.body.i.i.outer73.split.us.while.body.i.i.outer73.split.us.split_crit_edge ]
  %call11.i.i = tail call i32 (...)* @nondet_int() nounwind, !dbg !114
  tail call void @llvm.dbg.value(metadata !{i32 %call11.i.i}, i64 0, metadata !116), !dbg !114
  tail call void @llvm.dbg.value(metadata !{i32 %call11.i.i}, i64 0, metadata !116), !dbg !114
  tail call void @llvm.dbg.value(metadata !{i32 %call11.i.i}, i64 0, metadata !116), !dbg !117
  %tobool12.i.i = icmp eq i32 %call11.i.i, 0, !dbg !117
  br i1 %tobool12.i.i, label %while.body.i.i.outer73, label %if.then13.i.i, !dbg !117

if.then13.i.i:                                    ; preds = %if.then10.i.i
  br i1 %cmp.i2.i.i.i.i.i, label %ERROR.i.i.i.i, label %while.body.i.i.outer, !dbg !118

ERROR.i.i.i.i:                                    ; preds = %if.then13.i.i
  ret i32 5

eval.exit.i:                                      ; preds = %if.else.i.i31.i
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !124
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !124
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !125
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !104
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !105), !dbg !106
  tail call void @llvm.dbg.value(metadata !126, i64 0, metadata !49), !dbg !127
  tail call void @llvm.dbg.value(metadata !128, i64 0, metadata !49), !dbg !129
  tail call void @llvm.dbg.value(metadata !128, i64 0, metadata !49), !dbg !129
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !54), !dbg !130
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !68), !dbg !133
  %cmp.i82.i = icmp eq i32 %.26.lcssa, 0, !dbg !135
  br i1 %cmp.i82.i, label %while.body.i.i.outer, label %if.then.i, !dbg !135

if.then.i:                                        ; preds = %eval.exit.i
  %cmp.i.i53.i = icmp eq i32 %.lcssa, 1, !dbg !137
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !138
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !138
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !139
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !140), !dbg !136
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !140), !dbg !136
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !140), !dbg !141
  tail call void @llvm.dbg.value(metadata !142, i64 0, metadata !49), !dbg !143
  tail call void @llvm.dbg.value(metadata !142, i64 0, metadata !49), !dbg !143
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !54), !dbg !145
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !68), !dbg !148
  %cmp.i.i.i126 = icmp eq i32 %.26.lcssa, 0, !dbg !150
  %cmp.i.i.i = or i1 %cmp.i.i53.i, %cmp.i.i.i126, !dbg !150
  br i1 %cmp.i.i.i, label %while.body.i.i.outer, label %start_simulation.exit, !dbg !150

start_simulation.exit:                            ; preds = %if.then.i
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !155
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !155
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !123), !dbg !156
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !157), !dbg !151
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !157), !dbg !151
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !157), !dbg !158
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !159), !dbg !160
  tail call void @llvm.dbg.value(metadata !2, i64 0, metadata !159), !dbg !161
  br label %_UFO__exit, !dbg !161

_UFO__exit:                                       ; preds = %_UFO__exit, %start_simulation.exit
  br label %_UFO__exit
}

declare i32 @exit(i32)

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 720913, i32 0, i32 12, metadata !"tests/bench2/systemc/transmitter.01.BUG.cil.c.CIL.c", metadata !"/ag/svn/interp/trunk/srcufo/src/lib/LBE", metadata !"clang version 3.0 (branches/release_30 146682)", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !32} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !11, metadata !12, metadata !13, metadata !17, metadata !18, metadata !19, metadata !20, metadata !21, metadata !22, metadata !23, metadata !24, metadata !25, metadata !26, metadata !27, metadata !28, metadata !29, metadata !30, metadata !31}
!5 = metadata !{i32 720942, i32 0, metadata !6, metadata !"error", metadata !"error", metadata !"", metadata !6, i32 5, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 720937, metadata !"transmitter.01.BUG.cil.c", metadata !"/ag/svn/interp/trunk/srcufo/src/lib/LBE", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 720917, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i32 0, i32 0, i32 0, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{null}
!9 = metadata !{metadata !10}
!10 = metadata !{i32 720932}                      ; [ DW_TAG_base_type ]
!11 = metadata !{i32 720942, i32 0, metadata !6, metadata !"master", metadata !"master", metadata !"", metadata !6, i32 26, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!12 = metadata !{i32 720942, i32 0, metadata !6, metadata !"transmit1", metadata !"transmit1", metadata !"", metadata !6, i32 67, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!13 = metadata !{i32 720942, i32 0, metadata !6, metadata !"is_master_triggered", metadata !"is_master_triggered", metadata !"", metadata !6, i32 100, metadata !14, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!14 = metadata !{i32 720917, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i32 0, i32 0, i32 0, metadata !15, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!15 = metadata !{metadata !16}
!16 = metadata !{i32 720932, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!17 = metadata !{i32 720942, i32 0, metadata !6, metadata !"is_transmit1_triggered", metadata !"is_transmit1_triggered", metadata !"", metadata !6, i32 119, metadata !14, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!18 = metadata !{i32 720942, i32 0, metadata !6, metadata !"update_channels", metadata !"update_channels", metadata !"", metadata !6, i32 138, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 720942, i32 0, metadata !6, metadata !"init_threads", metadata !"init_threads", metadata !"", metadata !6, i32 146, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!20 = metadata !{i32 720942, i32 0, metadata !6, metadata !"exists_runnable_thread", metadata !"exists_runnable_thread", metadata !"", metadata !6, i32 164, metadata !14, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!21 = metadata !{i32 720942, i32 0, metadata !6, metadata !"eval", metadata !"eval", metadata !"", metadata !6, i32 184, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!22 = metadata !{i32 720942, i32 0, metadata !6, metadata !"fire_delta_events", metadata !"fire_delta_events", metadata !"", metadata !6, i32 235, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!23 = metadata !{i32 720942, i32 0, metadata !6, metadata !"reset_delta_events", metadata !"reset_delta_events", metadata !"", metadata !6, i32 258, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!24 = metadata !{i32 720942, i32 0, metadata !6, metadata !"activate_threads", metadata !"activate_threads", metadata !"", metadata !6, i32 281, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!25 = metadata !{i32 720942, i32 0, metadata !6, metadata !"immediate_notify", metadata !"immediate_notify", metadata !"", metadata !6, i32 306, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!26 = metadata !{i32 720942, i32 0, metadata !6, metadata !"fire_time_events", metadata !"fire_time_events", metadata !"", metadata !6, i32 317, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!27 = metadata !{i32 720942, i32 0, metadata !6, metadata !"reset_time_events", metadata !"reset_time_events", metadata !"", metadata !6, i32 326, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!28 = metadata !{i32 720942, i32 0, metadata !6, metadata !"init_model", metadata !"init_model", metadata !"", metadata !6, i32 349, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!29 = metadata !{i32 720942, i32 0, metadata !6, metadata !"stop_simulation", metadata !"stop_simulation", metadata !"", metadata !6, i32 359, metadata !14, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!30 = metadata !{i32 720942, i32 0, metadata !6, metadata !"start_simulation", metadata !"start_simulation", metadata !"", metadata !6, i32 378, metadata !7, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, null, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!31 = metadata !{i32 720942, i32 0, metadata !6, metadata !"main", metadata !"main", metadata !"", metadata !6, i32 437, metadata !14, i1 false, i1 true, i32 0, i32 0, i32 0, i32 256, i1 false, i32 ()* @main, null, null, metadata !9} ; [ DW_TAG_subprogram ]
!32 = metadata !{metadata !33}
!33 = metadata !{metadata !34, metadata !35, metadata !36, metadata !37, metadata !38, metadata !39, metadata !40, metadata !41, metadata !42}
!34 = metadata !{i32 720948, i32 0, null, metadata !"m_pc", metadata !"m_pc", metadata !"", metadata !6, i32 13, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!35 = metadata !{i32 720948, i32 0, null, metadata !"t1_pc", metadata !"t1_pc", metadata !"", metadata !6, i32 14, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!36 = metadata !{i32 720948, i32 0, null, metadata !"M_E", metadata !"M_E", metadata !"", metadata !6, i32 19, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!37 = metadata !{i32 720948, i32 0, null, metadata !"T1_E", metadata !"T1_E", metadata !"", metadata !6, i32 20, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!38 = metadata !{i32 720948, i32 0, null, metadata !"E_1", metadata !"E_1", metadata !"", metadata !6, i32 21, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!39 = metadata !{i32 720948, i32 0, null, metadata !"m_st", metadata !"m_st", metadata !"", metadata !6, i32 15, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!40 = metadata !{i32 720948, i32 0, null, metadata !"t1_st", metadata !"t1_st", metadata !"", metadata !6, i32 16, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!41 = metadata !{i32 720948, i32 0, null, metadata !"m_i", metadata !"m_i", metadata !"", metadata !6, i32 17, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!42 = metadata !{i32 720948, i32 0, null, metadata !"t1_i", metadata !"t1_i", metadata !"", metadata !6, i32 18, metadata !16, i32 0, i32 1, null} ; [ DW_TAG_variable ]
!43 = metadata !{i32 438, i32 10, metadata !44, null}
!44 = metadata !{i32 720907, metadata !45, i32 439, i32 3, metadata !6, i32 75} ; [ DW_TAG_lexical_block ]
!45 = metadata !{i32 720907, metadata !31, i32 437, i32 1, metadata !6, i32 74} ; [ DW_TAG_lexical_block ]
!46 = metadata !{i32 439, i32 11, metadata !44, null}
!47 = metadata !{i32 440, i32 9, metadata !44, null}
!48 = metadata !{i32 441, i32 10, metadata !44, null}
!49 = metadata !{i32 721152, metadata !50, metadata !"kernel_st", metadata !6, i32 378, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!50 = metadata !{i32 720907, metadata !30, i32 378, i32 1, metadata !6, i32 69} ; [ DW_TAG_lexical_block ]
!51 = metadata !{i32 384, i32 3, metadata !52, metadata !53}
!52 = metadata !{i32 720907, metadata !50, i32 382, i32 3, metadata !6, i32 70} ; [ DW_TAG_lexical_block ]
!53 = metadata !{i32 446, i32 3, metadata !44, null}
!54 = metadata !{i32 721152, metadata !55, metadata !"__retres1", metadata !6, i32 100, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!55 = metadata !{i32 720907, metadata !13, i32 100, i32 1, metadata !6, i32 13} ; [ DW_TAG_lexical_block ]
!56 = metadata !{i32 113, i32 3, metadata !57, metadata !58}
!57 = metadata !{i32 720907, metadata !55, i32 102, i32 3, metadata !6, i32 14} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 286, i32 9, metadata !59, metadata !61}
!59 = metadata !{i32 720907, metadata !60, i32 284, i32 3, metadata !6, i32 52} ; [ DW_TAG_lexical_block ]
!60 = metadata !{i32 720907, metadata !24, i32 281, i32 1, metadata !6, i32 51} ; [ DW_TAG_lexical_block ]
!61 = metadata !{i32 388, i32 3, metadata !52, metadata !53}
!62 = metadata !{i32 105, i32 7, metadata !63, metadata !58}
!63 = metadata !{i32 720907, metadata !64, i32 104, i32 19, metadata !6, i32 16} ; [ DW_TAG_lexical_block ]
!64 = metadata !{i32 720907, metadata !57, i32 103, i32 18, metadata !6, i32 15} ; [ DW_TAG_lexical_block ]
!65 = metadata !{i32 115, i32 3, metadata !57, metadata !58}
!66 = metadata !{i32 721152, metadata !60, metadata !"tmp", metadata !6, i32 281, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!67 = metadata !{i32 288, i32 3, metadata !59, metadata !61}
!68 = metadata !{i32 721152, metadata !69, metadata !"__retres1", metadata !6, i32 119, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!69 = metadata !{i32 720907, metadata !17, i32 119, i32 1, metadata !6, i32 17} ; [ DW_TAG_lexical_block ]
!70 = metadata !{i32 132, i32 3, metadata !71, metadata !72}
!71 = metadata !{i32 720907, metadata !69, i32 121, i32 3, metadata !6, i32 18} ; [ DW_TAG_lexical_block ]
!72 = metadata !{i32 294, i32 13, metadata !59, metadata !61}
!73 = metadata !{i32 124, i32 7, metadata !74, metadata !72}
!74 = metadata !{i32 720907, metadata !75, i32 123, i32 19, metadata !6, i32 20} ; [ DW_TAG_lexical_block ]
!75 = metadata !{i32 720907, metadata !71, i32 122, i32 19, metadata !6, i32 19} ; [ DW_TAG_lexical_block ]
!76 = metadata !{i32 134, i32 3, metadata !71, metadata !72}
!77 = metadata !{i32 721152, metadata !60, metadata !"tmp___0", metadata !6, i32 282, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!78 = metadata !{i32 296, i32 3, metadata !59, metadata !61}
!79 = metadata !{i32 271, i32 3, metadata !80, metadata !82}
!80 = metadata !{i32 720907, metadata !81, i32 260, i32 3, metadata !6, i32 47} ; [ DW_TAG_lexical_block ]
!81 = metadata !{i32 720907, metadata !23, i32 258, i32 1, metadata !6, i32 46} ; [ DW_TAG_lexical_block ]
!82 = metadata !{i32 389, i32 3, metadata !52, metadata !53}
!83 = metadata !{i32 122, i32 3, metadata !71, metadata !84}
!84 = metadata !{i32 294, i32 13, metadata !59, metadata !85}
!85 = metadata !{i32 310, i32 3, metadata !86, metadata !88}
!86 = metadata !{i32 720907, metadata !87, i32 308, i32 3, metadata !6, i32 56} ; [ DW_TAG_lexical_block ]
!87 = metadata !{i32 720907, metadata !25, i32 306, i32 1, metadata !6, i32 55} ; [ DW_TAG_lexical_block ]
!88 = metadata !{i32 44, i32 5, metadata !89, metadata !92}
!89 = metadata !{i32 720907, metadata !90, i32 40, i32 13, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!90 = metadata !{i32 720907, metadata !91, i32 28, i32 3, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!91 = metadata !{i32 720907, metadata !11, i32 26, i32 1, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!92 = metadata !{i32 205, i32 9, metadata !93, metadata !98}
!93 = metadata !{i32 720907, metadata !94, i32 202, i32 22, metadata !6, i32 38} ; [ DW_TAG_lexical_block ]
!94 = metadata !{i32 720907, metadata !95, i32 199, i32 20, metadata !6, i32 37} ; [ DW_TAG_lexical_block ]
!95 = metadata !{i32 720907, metadata !96, i32 189, i32 13, metadata !6, i32 35} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 720907, metadata !97, i32 188, i32 3, metadata !6, i32 34} ; [ DW_TAG_lexical_block ]
!97 = metadata !{i32 720907, metadata !21, i32 184, i32 1, metadata !6, i32 33} ; [ DW_TAG_lexical_block ]
!98 = metadata !{i32 396, i32 5, metadata !99, metadata !53}
!99 = metadata !{i32 720907, metadata !52, i32 392, i32 13, metadata !6, i32 71} ; [ DW_TAG_lexical_block ]
!100 = metadata !{i32 213, i32 5, metadata !95, metadata !98}
!101 = metadata !{i32 167, i32 3, metadata !102, metadata !104}
!102 = metadata !{i32 720907, metadata !103, i32 166, i32 3, metadata !6, i32 30} ; [ DW_TAG_lexical_block ]
!103 = metadata !{i32 720907, metadata !20, i32 164, i32 1, metadata !6, i32 29} ; [ DW_TAG_lexical_block ]
!104 = metadata !{i32 192, i32 11, metadata !95, metadata !98}
!105 = metadata !{i32 721152, metadata !97, metadata !"tmp", metadata !6, i32 184, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!106 = metadata !{i32 194, i32 5, metadata !95, metadata !98}
!107 = metadata !{i32 201, i32 19, metadata !94, metadata !98}
!108 = metadata !{i32 721152, metadata !97, metadata !"tmp_ndt_1", metadata !6, i32 185, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!109 = metadata !{i32 202, i32 7, metadata !94, metadata !98}
!110 = metadata !{i32 29, i32 3, metadata !90, metadata !92}
!111 = metadata !{i32 32, i32 5, metadata !112, metadata !92}
!112 = metadata !{i32 720907, metadata !90, i32 29, i32 22, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!113 = metadata !{i32 171, i32 3, metadata !102, metadata !104}
!114 = metadata !{i32 215, i32 19, metadata !115, metadata !98}
!115 = metadata !{i32 720907, metadata !95, i32 213, i32 21, metadata !6, i32 39} ; [ DW_TAG_lexical_block ]
!116 = metadata !{i32 721152, metadata !97, metadata !"tmp_ndt_2", metadata !6, i32 186, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!117 = metadata !{i32 216, i32 7, metadata !115, metadata !98}
!118 = metadata !{i32 70, i32 3, metadata !119, metadata !121}
!119 = metadata !{i32 720907, metadata !120, i32 69, i32 3, metadata !6, i32 9} ; [ DW_TAG_lexical_block ]
!120 = metadata !{i32 720907, metadata !12, i32 67, i32 1, metadata !6, i32 8} ; [ DW_TAG_lexical_block ]
!121 = metadata !{i32 219, i32 9, metadata !122, metadata !98}
!122 = metadata !{i32 720907, metadata !115, i32 216, i32 22, metadata !6, i32 40} ; [ DW_TAG_lexical_block ]
!123 = metadata !{i32 721152, metadata !103, metadata !"__retres1", metadata !6, i32 164, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!124 = metadata !{i32 178, i32 3, metadata !102, metadata !104}
!125 = metadata !{i32 180, i32 3, metadata !102, metadata !104}
!126 = metadata !{i32 2}
!127 = metadata !{i32 399, i32 5, metadata !99, metadata !53}
!128 = metadata !{i32 3}
!129 = metadata !{i32 403, i32 5, metadata !99, metadata !53}
!130 = metadata !{i32 113, i32 3, metadata !57, metadata !131}
!131 = metadata !{i32 286, i32 9, metadata !59, metadata !132}
!132 = metadata !{i32 405, i32 5, metadata !99, metadata !53}
!133 = metadata !{i32 132, i32 3, metadata !71, metadata !134}
!134 = metadata !{i32 294, i32 13, metadata !59, metadata !132}
!135 = metadata !{i32 167, i32 3, metadata !102, metadata !136}
!136 = metadata !{i32 409, i32 11, metadata !99, metadata !53}
!137 = metadata !{i32 103, i32 3, metadata !57, metadata !131}
!138 = metadata !{i32 178, i32 3, metadata !102, metadata !136}
!139 = metadata !{i32 180, i32 3, metadata !102, metadata !136}
!140 = metadata !{i32 721152, metadata !50, metadata !"tmp", metadata !6, i32 379, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!141 = metadata !{i32 411, i32 5, metadata !99, metadata !53}
!142 = metadata !{i32 4}
!143 = metadata !{i32 413, i32 7, metadata !144, metadata !53}
!144 = metadata !{i32 720907, metadata !99, i32 411, i32 19, metadata !6, i32 72} ; [ DW_TAG_lexical_block ]
!145 = metadata !{i32 113, i32 3, metadata !57, metadata !146}
!146 = metadata !{i32 286, i32 9, metadata !59, metadata !147}
!147 = metadata !{i32 415, i32 7, metadata !144, metadata !53}
!148 = metadata !{i32 132, i32 3, metadata !71, metadata !149}
!149 = metadata !{i32 294, i32 13, metadata !59, metadata !147}
!150 = metadata !{i32 167, i32 3, metadata !102, metadata !151}
!151 = metadata !{i32 364, i32 9, metadata !152, metadata !154}
!152 = metadata !{i32 720907, metadata !153, i32 362, i32 3, metadata !6, i32 67} ; [ DW_TAG_lexical_block ]
!153 = metadata !{i32 720907, metadata !29, i32 359, i32 1, metadata !6, i32 66} ; [ DW_TAG_lexical_block ]
!154 = metadata !{i32 422, i32 15, metadata !99, metadata !53}
!155 = metadata !{i32 178, i32 3, metadata !102, metadata !151}
!156 = metadata !{i32 180, i32 3, metadata !102, metadata !151}
!157 = metadata !{i32 721152, metadata !153, metadata !"tmp", metadata !6, i32 359, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!158 = metadata !{i32 366, i32 3, metadata !152, metadata !154}
!159 = metadata !{i32 721152, metadata !45, metadata !"__retres1", metadata !6, i32 437, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!160 = metadata !{i32 448, i32 3, metadata !44, null}
!161 = metadata !{i32 449, i32 3, metadata !44, null}
